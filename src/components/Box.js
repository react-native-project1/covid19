import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const Box = (props) => {
  return (
    <View
      style={{
        backgroundColor: props.backgroundColor,
        width: props.width,
        height: 80,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5,
      }}>
      <Text style={styles.total}>{props.value}</Text>
      <Text style={styles.label}>{props.label} <Icon name={props.iconLabel} size={25} /></Text>
    </View>
  );
};

const styles = StyleSheet.create({
  total: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#FFFFFF',
  },
  label: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#FFFFFF',
  },
});

export default Box;