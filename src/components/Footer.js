import React from 'react';
import {View, Text, StyleSheet, Linking} from 'react-native';

const Footer = ({lastUpdate}) => {
  return (
    <View style={styles.footer}>
      <Text style={{textAlign: 'center', marginBottom: 5}}>
        Source :{' '}
        <Text
          style={{fontWeight: 'bold'}}
          onPress={() => {
            Linking.openURL('https://covid19.mathdro.id/api/');
          }}>
          https://covid19.mathdro.id/api/
        </Text>
      </Text>
      <Text style={{textAlign: 'center'}}>Pembaruan Terakhir</Text>
      <Text style={{textAlign: 'center'}}>
          {lastUpdate}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  footer: {
    position: 'absolute',
    bottom: 0,
    width: '100%',
    marginBottom: 10,
  },
});

export default Footer;
