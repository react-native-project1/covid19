import React from 'react'
import 'react-native-gesture-handler';
import {createStackNavigator} from 'react-navigation-stack';
import {Home, Countries, Country} from '../pages/index';
import {createAppContainer} from 'react-navigation';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { Icon } from 'native-base';

const HomeStack = createStackNavigator({
  Home: {
    screen: Home,
    navigationOptions: {
      title: 'PT. REBAHAN MELAWAN COVID-19',
      headerTitleAlign: 'center',
    },
  },
  // Refresh
});

const CountriesStack = createStackNavigator({
  Countries,
  Country
});

const Routes = createBottomTabNavigator(
  {
    HomeStack: {
      screen: HomeStack,
      navigationOptions: {
        title: 'Home',
        tabBarIcon: ({focused, tintColor}) => {
            let iconSize;
            iconSize = focused ? 30 : 25;
            return <Icon name="home" color={tintColor} style={{color: tintColor, fontSize: iconSize}} />
        }
      },
    },
    CountriesStack: {
      screen: CountriesStack,
      navigationOptions: {
        title: 'Countries',
        tabBarIcon: ({focused, tintColor}) => {
            let iconSize;
            iconSize = focused ? 30 : 25;
            return <Icon name="globe" color={tintColor} style={{color: tintColor, fontSize: iconSize}} />
        }
      },
    },
  },
  {
    initialRouteName: 'HomeStack',
    // defaultNavigationOptions: ({navigation}) => ({
    //   tabBarIcon: ({focused, horizontal, tintColor}) => {
    //     const {routeName} = navigation.state;
    //     let IconComponent = Icon;
    //     let iconName;
    //     if (routeName === 'HomeStack') {
    //       iconName = focused
    //         ? 'home'
    //         : 'home';
    //       // Sometimes we want to add badges to some icons.
    //       // You can check the implementation below.
    //     //   IconComponent = HomeIconWithBadge;
    //     } else if (routeName === 'Settings') {
    //       iconName = focused ? 'ios-list-box' : 'ios-list';
    //     }

    //     // You can return any component that you like here!
    //     return <IconComponent name={iconName} size={25} color={tintColor} style={{color: tintColor}} />;
    //   },
    // }),
    tabBarOptions: {
      activeTintColor: 'tomato',
      inactiveTintColor: 'gray',
    },
  },
);
export default createAppContainer(Routes);