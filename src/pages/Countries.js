import React from 'react';
import {View, FlatList, ActivityIndicator, TouchableOpacity} from 'react-native';
import {
  Header,
  Item,
  Input,
  Icon,
  Text,
  ListItem,
  Left,
  Right,
} from 'native-base';


class Countries extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,   
      data: [],
      temp: [],
      error: null,
      search: null
    };
  }

  getCountries = () => {
    this.setState({loading: true});
    fetch('https://covid19.mathdro.id/api/countries')
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          loading: false,
          data: responseJson.countries,
          temp: responseJson.countries,
        });
      })
      .catch((error) => {
        this.setState({ error: 'Error Loading content', loading: false });
      });
  }

  updateSearch = search => {
    this.setState({search}, () => {
      if ('' == search){
        this.setState({
          data: [...this.state.temp]
        })
        return;
      }

      this.state.data = this.state.temp.filter((item) => {
        return item.name.includes(search)
      }).map(({name}) => {
        return {name}
      })
    })
  };

  componentDidMount() {
    this.getCountries();
  }

  renderFooter = () => {
    if (!this.state.loading) return null;
    return (
      <View
        style={{paddingVertical: 20, borderTopWidth: 1, borderColor: 'pink'}}>
        <ActivityIndicator animating size="large" />
      </View>
    );
  };

  render() {
    return (
      <View>
        <Header searchBar rounded style={{backgroundColor: '#F1F1F1'}}>
          <Item>
            <Icon name="ios-search" />
            <Input
              placeholder="Search"
              onChangeText={this.updateSearch}
              value={this.state.search}
            />
            <Icon name="ios-flag" />
          </Item>
        </Header>
        <View>
          <FlatList
            // ListHeaderComponent={this.renderHeader}
            ListFooterComponent={this.renderFooter}
            data={this.state.data}
            keyExtractor={item => item.name}
            renderItem={({item}) => (
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate('Country', {
                    name: item.name,
                    id: item.iso2,
                  });
                }}>
                <ListItem>
                  <Left>
                    <Text>{item.name}</Text>
                  </Left>
                  <Right>
                    <Icon name="arrow-forward" />
                  </Right>
                </ListItem>
              </TouchableOpacity>
            )}
          />
        </View>
      </View>
    );
  }
}

export default Countries;
