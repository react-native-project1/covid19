import React from 'react';
import {
  View,
  Text,
  Image,
  ScrollView,
  RefreshControl,
  Alert,
} from 'react-native';
import 'react-native-gesture-handler';
import Footer from '../components/Footer';
import Box from '../components/Box';
import styles from '../assets/styles/MainStyles';

class Country extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      condition: true,
      refreshing: false,
      dataSource: [],
      countryId: JSON.stringify(this.props.navigation.getParam('id', 'NO-ID'))
        .split('"')
        .join(''),
    };
  }

  getData = async () => {
    fetch(`https://covid19.mathdro.id/api/countries/${this.state.countryId}`)
      .then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson);
        this.setState({
          dataSource: responseJson,
          condition: true,
        });
      })
      .catch((error) => {
        Alert.alert('Cek koneksi vrohhh...');
        this.setState({
          condition: false,
        });
      });
  };

  _onRefresh = () => {
    this.setState({refreshing: true});
    this.getData().then(() => {
      this.setState({refreshing: false});
    });
  };

  componentDidMount() {
    this.getData();
  }
  static navigationOptions = ({navigation}) => {
    return {
      title: navigation.getParam('name', 'Negara tak dikenal'),
    };
  };

  render() {
    var confirmed = {...this.state.dataSource.confirmed};
    var recovered = {...this.state.dataSource.recovered};
    var deaths = {...this.state.dataSource.deaths};
    const {navigation} = this.props;
    if (!this.state.condition) {
      return (
        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh}
            />
          }>
          <View
            style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <Text>Gagal memuat data...</Text>
            <Text style={{fontWeight: 'bold'}}>Ada paket ga ?</Text>
            <Text style={{fontWeight: 'bold'}}>Datanya dah nyala belum ?</Text>
            <Text style={{fontWeight: 'bold'}}>
              Pake wifi tetangga coba kalo gada paket!!!
            </Text>
          </View>
        </ScrollView>
      );
    } else {
      return (
        <View style={styles.container}>
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this._onRefresh}
              />
            }>
            <View style={styles.titleContainer}>
              <Text style={styles.title}>
                LAPORAN COVID-19{' '}
                {JSON.stringify(navigation.getParam('name', '-_-'))
                  .split('"')
                  .join('')}
              </Text>
            </View>
            <View style={styles.boxContainer}>
              <Box
                value={confirmed.value}
                iconLabel="emoticon-sad-outline"
                label="Terkonfirmasi"
                backgroundColor="#FFA500"
                width="47%"
              />
              <Box
                value={recovered.value}
                iconLabel="emoticon-kiss-outline"
                label="Sembuh"
                backgroundColor="#008000"
                width="47%"
              />
              <View style={{height: 10, width: '100%'}}></View>
              <Box
                value={deaths.value}
                iconLabel="emoticon-cry-outline"
                label="Meninggal"
                backgroundColor="#FF0000"
                width="100%"
              />
            </View>
            <View>
              <Image
                source={{uri: this.state.dataSource.image}}
                style={styles.poster}
              />
            </View>
          </ScrollView>
          <Footer lastUpdate={Date(this.state.dataSource.lastUpdate)} />
        </View>
      );
    }
  }
}

export default Country;
