import React from 'react';
import {View, FlatList, ActivityIndicator, TouchableOpacity} from 'react-native';
import {
  Header,
  Item,
  Input,
  Icon,
  Text,
  ListItem,
  Left,
  Right,
} from 'native-base';
import _ from '../../node_modules/lodash/index'


class Countries extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      data: [],
      fullData: [],
      error: null,
    };
  }

  getCountries = _.debounce(() => {
    this.setState({loading: true});
    fetch('https://covid19.mathdro.id/api/countries')
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          loading: false,
          data: responseJson.countries,
          fullData: responseJson.countries,
        });
      })
      .catch((error) => {
        this.setState({error, loading: false});
      });
  }, 250);

  handleSearch = (text) => {
    const formattedQuery = text.toLowerCase();
    const data = _.filter(this.state.fullData, negara => {
      if (negara.name.includes(formattedQuery)) {
        return true;
      }
      return false;
    });
    this.setState({data: data, query: text});
  };

  componentDidMount() {
    this.getCountries();
  }

  renderHeader = () => {
    return (
      <Header searchBar rounded style={{backgroundColor: '#F1F1F1'}}>
        <Item>
          <Icon name="ios-search" />
          <Input
            placeholder="Search"
            onChangeText={this.handleSearch}
          />
          <Icon name="ios-flag" />
        </Item>
      </Header>
    );
  };

  renderFooter = () => {
    if (!this.state.loading) return null;
    return (
      <View
        style={{paddingVertical: 20, borderTopWidth: 1, borderColor: 'pink'}}>
        <ActivityIndicator animating size="large" />
      </View>
    );
  };

  render() {
    return (
      <View>
        <Header searchBar rounded style={{backgroundColor: '#F1F1F1'}}>
          <Item>
            <Icon name="ios-search" />
            <Input
              placeholder="Search"
              onChangeText={this.handleSearch}
              value={this.state.query}
            />
            <Icon name="ios-flag" />
          </Item>
        </Header>
        <View>
          <FlatList
            // ListHeaderComponent={this.renderHeader}
            ListFooterComponent={this.renderFooter}
            data={this.state.data}
            keyExtractor={item => item.iso2}
            renderItem={({item}) => (
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate('Country', {
                    name: item.name,
                    id: item.iso2,
                  });
                }}>
                <ListItem>
                  <Left>
                    <Text>{item.name}</Text>
                  </Left>
                  <Right>
                    <Icon name="arrow-forward" />
                  </Right>
                </ListItem>
              </TouchableOpacity>
            )}
          />
        </View>
      </View>
    );
  }
}

export default Countries;
