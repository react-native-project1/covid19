import React from 'react';
import 'react-native-gesture-handler';
import Routes from './src/config/Routes';

class App extends React.Component {
    render(){
        return(
            <Routes />
        )
    }
}
export default App;